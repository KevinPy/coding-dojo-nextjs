import { withRouter } from 'next/router';
import Layout from '../components/layout';

const Post = withRouter((props) => (
  <Layout>
    <h1>{props.article.title}</h1>
    <p>{props.article.content}</p>
  </Layout>
));

Post.getInitialProps = async function (ctx) {
  const { id } = ctx.query;
  const data = await require("../data.json");
  const article = data.find(x => x.id === id);
  return {
    article
  };
}

export default Post;
