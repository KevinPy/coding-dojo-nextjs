import Layout from '../components/layout';

const About = () => (
  <Layout>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas ipsum quia vero? Facilis esse labore molestias quasi suscipit facere velit rerum nulla quisquam autem. Minus tenetur ex facere doloremque sit!</p>
  </Layout>
);

export default About;
