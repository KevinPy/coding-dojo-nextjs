import Layout from '../components/layout';
import Link from 'next/link';

const PostLink = (props) => (
  <li>
    <Link as={`/p/${props.value.id}`} href={`/post?id=${props.value.id}`}>
      <a>{props.value.title}</a>
    </Link>
  </li>
);

const Index = (props) => (
  <Layout>
    <h1>Coding Dojo Next.js</h1>
    <ul>
      {props.articles.map(article => (
        <PostLink key={article.id} value={article} />
      ))}
    </ul>
  </Layout>
);

Index.getInitialProps = async function () {
  const articles = await require("../data.json");
  return {
    articles
  };
};

export default Index;
