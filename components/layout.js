import Header from '../components/header';

const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #888'
};

const Layout = (props) => (
  <main style={layoutStyle}>
    <Header />
    <section>
      {props.children}
    </section>
  </main>
);

export default Layout;
