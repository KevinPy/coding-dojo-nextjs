import Link from 'next/link';

const linkStyle = {
  marginRight: 15
};

const Header = () => (
  <header>
    <Link href="/">
      <a style={linkStyle}>Home Page</a>
    </Link>
    <Link href="/about">
      <a style={linkStyle}>About Page</a>
    </Link>
  </header>
);

export default Header;
